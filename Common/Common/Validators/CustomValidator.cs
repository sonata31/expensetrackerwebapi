﻿using FluentValidation;

namespace Common.Validators
{
    public static class CustomValidator
    {
        public static IRuleBuilderOptions<T, double> ValidateAmount<T>(this IRuleBuilder<T, double> ruleBuilder)
        {
            return (IRuleBuilderOptions<T, double>)ruleBuilder.Custom((amount, context) =>
            {
                if (amount <= 0)
                    context.AddFailure("Amount should be greater than 0.");
            });
        }
    }
}
