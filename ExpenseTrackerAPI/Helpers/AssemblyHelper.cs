﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ExpenseTrackerAPI
{
    public class AssemblyHelper
    {
        public static List<Assembly> GetAssembly(params string[] assemblies)
        {
            List<Assembly> assemblyList = new List<Assembly>();

            var assemblys = AppDomain.CurrentDomain.GetAssemblies().Where(x => x.FullName.Contains("ExpenseTrackerAPI")).First();
            var refAssemblies = assemblys.GetReferencedAssemblies();

            foreach (string assembly in assemblies)
            {
                var refAssemblys = refAssemblies.Where(x => x.Name.Contains(assembly)).First();
                var reqAssembly = Assembly.Load(refAssemblys);
                assemblyList.Add(reqAssembly);
            }

            return assemblyList;
        }
    }
}
