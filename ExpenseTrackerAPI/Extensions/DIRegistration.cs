﻿using Accounts.Config;
using Payables.Config;
using Expenses.Config;
using Savings.Config;
using Microsoft.Extensions.DependencyInjection;

namespace ExpenseTrackerAPI
{
    public static class DIRegistration
    {
        public static IServiceCollection RegisterDependencies(this IServiceCollection services)
        {
            services.RegisterIdentity();
            services.RegisterMediatrHandlers(AssemblyHelper.GetAssembly("Accounts.Infra", "Payables.Infra", "Expenses.Infra", "Savings.Infra"));

            services.RegisterAccountsServices();
            services.RegisterAccountsBusinessModels();
            services.ConfigureAccountsMappingProfile();

            services.RegisterPayablesServices();
            services.RegisterPayablesBusinessModels();
            services.ConfigurePayablesMappingProfile();

            services.RegisterExpensesServices();
            services.RegisterExpensesBusinessModels();
            services.ConfigureExpensesMappingProfile();

            services.RegisterSavingsServices();
            services.RegisterSavingsBusinessModels();
            services.ConfigureSavingsMappingProfile();

            return services;
        }
    }
}
