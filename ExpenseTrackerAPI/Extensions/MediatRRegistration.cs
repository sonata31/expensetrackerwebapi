﻿using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Reflection;

namespace ExpenseTrackerAPI
{
    public static class MediatRRegistration
    {
        public static IServiceCollection RegisterMediatrHandlers(this IServiceCollection services, List<Assembly> assembly)
        {
            services.AddMvc().AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblies(assembly));
            services.AddMediatR(assembly.ToArray());

            return services;
        }
    }
}
