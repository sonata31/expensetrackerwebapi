﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ExpenseTrackerAPI
{
    public static class CorsRegistration
    {
        public static IServiceCollection RegisterCORS(this IServiceCollection services, IConfiguration config)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(
                    config.GetSection("CorsName").Value,
                    builder =>
                    {
                        builder.WithOrigins(config.GetSection("Origin").Value)
                               .AllowAnyMethod()
                               .AllowAnyHeader();
                    }
                );
            });

            return services;
        }
    }
}
