﻿using Common;
using Expenses;

using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExpenseTrackerAPI.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/v1/expenses")]
    [ApiController]
    public class ExpensesController : BaseApiController
    {
        public ExpensesController(IMediator mediator) : base(mediator) { }

        [HttpPost]
        [Route("")]
        public async Task<Response> PostAddExpensesAsync([FromBody] AddExpensesCommand command) => await _mediator.Send(command);

        [HttpGet]
        [Route("{id:long}")]
        public async Task<List<ExpensesDTO>> GetExpensesAsync(long id) => await _mediator.Send(new GetExpensesQuery { UserId = id });
    }
}
