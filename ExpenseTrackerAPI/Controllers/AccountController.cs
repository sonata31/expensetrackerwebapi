﻿using Accounts;
using Common;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using MediatR;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace ExpenseTrackerAPI.Controllers
{
    [Authorize]
    [EnableCors("CorsPolicy")]
    [Route("api/v1/Account")]
    [ApiController]
    public class AccountController : BaseApiController
    {
        public AccountController(IMediator mediator) : base(mediator) { }

        [AllowAnonymous]
        [HttpPost]
        [Route("admin/register")]
        public async Task<Response> PostRegisterAsync([FromBody] CreateAdminUserCommand command) => await _mediator.Send(command);

        //[AllowAnonymous]
        //[HttpPost]
        //[Route("register")]
        //public async Task<Response> PostRegisterAsync([FromBody] CreateUserCommand command) => await _mediator.Send(command);

        [AllowAnonymous]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(TokenModel), 200)]
        [Route("login")]
        public async Task<TokenModel> PostLoginAsync([FromBody] LoginUserCommand command) => await _mediator.Send(command);
    }
}