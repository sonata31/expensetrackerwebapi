﻿using Common;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Savings;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExpenseTrackerAPI.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/v1/savings")]
    [ApiController]
    public class SavingsController : BaseApiController
    {
        public SavingsController(IMediator mediator) : base(mediator) { }

        [HttpPost]
        [Route("")]
        public async Task<Response> PostAddExpensesAsync([FromBody] AddSavingsCommand command) => await _mediator.Send(command);

        [HttpGet]
        [Route("{id:long}")]
        public async Task<List<SavingsDTO>> GetSavingsAsync(long id) => await _mediator.Send(new GetSavingsQuery { UserId = id });
    }
}
