﻿using Common;
using Payables;

using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace ExpenseTrackerAPI.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/v1/payables")]
    [ApiController]
    public class PayablesController : BaseApiController
    {
        public PayablesController(IMediator mediator) : base(mediator) { }

        [HttpPost]
        [Route("")]
        public async Task<Response> PostAddPayablesAsync([FromBody] AddPayablesCommand command)
            => await _mediator.Send(command);

        [HttpGet]
        [Route("{id:long}")]
        public async Task<List<PayablesDTO>> GetPayablesAsync(long id)
            => await _mediator.Send(new GetPayablesQuery { UserId = id });

        [HttpPost]
        [Route("pay")]
        public async Task<Response> PostPayPayableAsync([FromBody] LogPayableCommand command)
            => await _mediator.Send(command);

        [HttpGet]
        [Route("history/{id:long}")]
        public async Task<List<PayHistoryDTO>> GetPayHistoryAsync(long id)
            => await _mediator.Send(new GetPayHistoryQuery { UserId = id });

        [HttpGet]
        [Route("filter")]
        public async Task<List<PayHistoryDTO>> GetFilterPayHistoryAsync([FromQuery] FilterPayablesQuery request)
            => await _mediator.Send(new FilterPayablesQuery { UserId = request.UserId, PayableId = request.PayableId });
    }
}
