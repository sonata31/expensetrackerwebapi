﻿using Core;

using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace Expenses.Config
{
    public class AutoMapperRegistration : Profile
    {
        public AutoMapperRegistration()
        {
            CreateMap<ExpensesModel, Expense>();
        }
    }

    public static class MapperRegistration
    {
        public static IServiceCollection ConfigureExpensesMappingProfile(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(MapperRegistration));

            return services;
        }
    }
}
