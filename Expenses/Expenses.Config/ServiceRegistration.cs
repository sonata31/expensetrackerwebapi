﻿using Microsoft.Extensions.DependencyInjection;

namespace Expenses.Config
{
    public static class ServiceRegistration
    {
        public static IServiceCollection RegisterExpensesServices(this IServiceCollection services)
        {
            services.AddTransient<IExpenseRepository, ExpenseRepository>();
            services.AddTransient<IExpenseService, ExpenseService>();

            return services;
        }
    }
}
