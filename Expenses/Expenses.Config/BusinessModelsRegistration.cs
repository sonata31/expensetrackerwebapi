﻿using Common;
using Microsoft.Extensions.DependencyInjection;

namespace Expenses.Config
{
    public static class BusinessModelsRegistration
    {
        public static IServiceCollection RegisterExpensesBusinessModels(this IServiceCollection services)
        {
            services.AddSingleton<Response>();

            return services;
        }
    }
}
