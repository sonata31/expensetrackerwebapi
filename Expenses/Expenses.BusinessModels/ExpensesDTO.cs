﻿using System;

namespace Expenses
{
    public class ExpensesDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string StorePurchased { get; set; }
        public DateTime DateOfPurchase { get; set; }
        public decimal Amount { get; set; }
    }
}
