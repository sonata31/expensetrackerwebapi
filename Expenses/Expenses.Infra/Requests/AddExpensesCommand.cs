﻿using Common;
using MediatR;
using System;

namespace Expenses
{
    public class AddExpensesCommand : IRequest<Response>
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string StorePurchased { get; set; }
        public DateTime DateOfPurchase { get; set; }
        public double Amount { get; set; }
    }
}
