﻿using MediatR;
using System.Collections.Generic;

namespace Expenses
{
    public class GetExpensesQuery : IRequest<List<ExpensesDTO>>
    {
        public long UserId { get; set; }
    }
}
