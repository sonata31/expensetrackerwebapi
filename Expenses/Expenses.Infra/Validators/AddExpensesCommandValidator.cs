﻿using FluentValidation;
using Common.Validators;

namespace Expenses
{
    public class AddExpensesCommandValidator : AbstractValidator<AddExpensesCommand>
    {
        public AddExpensesCommandValidator()
        {
            RuleFor(x => x.UserId)
                .NotEmpty()
                .WithMessage("Please provide user id.");

            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage("Name field is mandatory.");

            RuleFor(x => x.DateOfPurchase)
                .NotEmpty()
                .WithMessage("Date purchase field is mandatory.");

            RuleFor(x => x.Amount)
                .NotEmpty()
                .WithMessage("Amount field is mandatory.")
                .ValidateAmount();
        }
    }
}
