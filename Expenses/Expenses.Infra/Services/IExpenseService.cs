﻿using Common;
using Core;

using AutoMapper;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Expenses
{
    public interface IExpenseService
    {
        Task<Response> AddExpensesAsync(ExpensesModel model);
        Task<List<ExpensesDTO>> GetExpensesAsync(long uid);
    }

    public class ExpenseService : IExpenseService
    {
        private readonly IMapper _mapper;
        private readonly IExpenseRepository _repo;
        private Response _response;

        public ExpenseService(
            IMapper mapper,
            IExpenseRepository repo,
            Response response)
        {
            _mapper = mapper;
            _repo = repo;
            _response = response;
        }

        public async Task<Response> AddExpensesAsync(ExpensesModel model)
        {
            var expenses = _mapper.Map<Expense>(model);
            expenses.DateCreated = DateTime.UtcNow;

            await _repo.AddExpensesAsync(expenses);

            _response.StatusCode = HttpStatusCode.OK;
            _response.Message = "New expense added.";

            return _response;
        }

        public async Task<List<ExpensesDTO>> GetExpensesAsync(long uid)
            => await _repo.GetExpensesAsync(uid);
    }
}
