﻿using Common;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Expenses
{
    public class AddExpenseHandler : IRequestHandler<AddExpensesCommand, Response>
    {
        private readonly IExpenseService _expenseSvc;

        public AddExpenseHandler(IExpenseService expenseSvc)
        {
            _expenseSvc = expenseSvc;
        }

        public async Task<Response> Handle(AddExpensesCommand request, CancellationToken cancellationToken)
        {
            var expenses = new ExpensesModel
            {
                UserId = request.UserId,
                Name = request.Name,
                StorePurchased = request.StorePurchased,
                DateOfPurchase = request.DateOfPurchase,
                Amount = Convert.ToDecimal(request.Amount)
            };

            return await _expenseSvc.AddExpensesAsync(expenses);
        }
    }
}
