﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Expenses
{
    public class GetExpensesHandler : IRequestHandler<GetExpensesQuery, List<ExpensesDTO>>
    {
        private readonly IExpenseService _expenseSvc;

        public GetExpensesHandler(IExpenseService expenseSvc)
        {
            _expenseSvc = expenseSvc;
        }

        public async Task<List<ExpensesDTO>> Handle(GetExpensesQuery request, CancellationToken cancellationToken)
            => await _expenseSvc.GetExpensesAsync(request.UserId);
    }
}
