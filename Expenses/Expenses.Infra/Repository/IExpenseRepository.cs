﻿using Core;
using Microsoft.Extensions.Configuration;
using Persistence;
using Persistence.Config;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Expenses
{
    public interface IExpenseRepository
    {
        Task AddExpensesAsync(Expense model);
        Task<List<ExpensesDTO>> GetExpensesAsync(long uid);
    }

    public class ExpenseRepository : IExpenseRepository
    {
        private readonly DbContext _db;
        private readonly IConfiguration _config;

        public ExpenseRepository(DbContext db, IConfiguration config)
        {
            _db = db;
            _config = config;
        }

        public async Task AddExpensesAsync(Expense model)
        {
            _db.Expenses.Add(model);
            await _db.SaveChangesAsync();
        }

        public async Task<List<ExpensesDTO>> GetExpensesAsync(long uid)
            => QueriesService.QueryDb<ExpensesDTO>(_config, "usp_GetExpenses", new Dictionary<string, object> { { "UserId", uid } });

    }
}
