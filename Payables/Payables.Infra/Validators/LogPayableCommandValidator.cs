﻿using FluentValidation;
using Common.Validators;

namespace Payables
{
    public class LogPayableCommandValidator : AbstractValidator<LogPayableCommand>
    {
        public LogPayableCommandValidator()
        {
            RuleFor(x => x.PaidFor)
                .NotEmpty()
                .WithMessage("Paid for field is mandatory.");

            RuleFor(x => x.Amount)
                .NotEmpty()
                .WithMessage("Amount field is mandatory.")
                .ValidateAmount();

            RuleFor(x => x.ModeOfPayment)
                .NotEmpty()
                .WithMessage("Mode of Payment field is mandatory.");

            RuleFor(x => x.DatePaid)
                .NotEmpty()
                .WithMessage("Date of Payment field is mandatory.");

            RuleFor(x => x.ReferenceNo)
                .NotEmpty()
                .WithMessage("Reference No. field is mandatory.");

            RuleFor(x => x.PayableId)
                .GreaterThanOrEqualTo(0)
                .WithMessage("Payable id field is mandatory.")
                .NotEmpty()
                .WithMessage("Payable id field is mandatory.");
        }
    }
}
