﻿using FluentValidation;
using Common.Validators;

namespace Payables
{
    public class AddPayablesCommandValidator : AbstractValidator<AddPayablesCommand>
    {
        public AddPayablesCommandValidator()
        {
            RuleFor(x => x.UserId)
                .NotEmpty()
                .WithMessage("Please provide user id.");

            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage("Name field is mandatory.");

            RuleFor(x => x.Amount)
                .NotEmpty()
                .WithMessage("Amount field is mandatory.")
                .ValidateAmount();
        }
    }
}
