﻿using MediatR;
using System.Collections.Generic;

namespace Payables
{
    public class GetPayHistoryQuery : IRequest<List<PayHistoryDTO>>
    {
        public long UserId { get; set; }
    }
}
