﻿using Common;
using MediatR;
using System;

namespace Payables
{
    public class LogPayableCommand : IRequest<Response>
    {
        public string PaidFor { get; set; }
        public double Amount { get; set; }
        public string CoverageFrom { get; set; }
        public int? StartYear { get; set; }
        public string CoverageTo { get; set; }
        public int? EndYear { get; set; }
        public string ModeOfPayment { get; set; }
        public DateTime DatePaid { get; set; }
        public string ReferenceNo { get; set; }
        public long PayableId { get; set; }
    }
}
