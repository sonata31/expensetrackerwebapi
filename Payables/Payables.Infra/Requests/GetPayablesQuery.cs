﻿using MediatR;
using System.Collections.Generic;

namespace Payables
{
    public class GetPayablesQuery : IRequest<List<PayablesDTO>>
    {
        public long UserId { get; set; }
    }
}
