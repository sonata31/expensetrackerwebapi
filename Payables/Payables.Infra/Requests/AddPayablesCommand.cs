﻿using Common;
using MediatR;

namespace Payables
{
    public class AddPayablesCommand : IRequest<Response>
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public double Amount { get; set; }
    }
}
