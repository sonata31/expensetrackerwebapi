﻿using MediatR;
using System.Collections.Generic;

namespace Payables
{
    public class FilterPayablesQuery : IRequest<List<PayHistoryDTO>>
    {
        public long UserId { get; set; }
        public long PayableId { get; set; }
    }
}
