﻿using Common;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Payables
{
    public class AddPayablesHandler : IRequestHandler<AddPayablesCommand, Response>
    {
        private readonly IPayablesService _payableSvc;

        public AddPayablesHandler(IPayablesService payablesSvc)
        {
            _payableSvc = payablesSvc;
        }

        public async Task<Response> Handle(AddPayablesCommand request, CancellationToken cancellationToken)
        {
            var payables = new PayablesModel
            {
                UserId = request.UserId,
                Name = request.Name,
                Amount = Convert.ToDecimal(request.Amount)
            };

            return await _payableSvc.AddPayablesAsync(payables);
        }
    }
}
