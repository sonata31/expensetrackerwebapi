﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Payables
{
    public class GetPayHistoryHandler : IRequestHandler<GetPayHistoryQuery, List<PayHistoryDTO>>
    {
        private readonly IPayablesService _payableSvc;

        public GetPayHistoryHandler(IPayablesService payablesSvc)
        {
            _payableSvc = payablesSvc;
        }

        public async Task<List<PayHistoryDTO>> Handle(GetPayHistoryQuery request, CancellationToken cancellationToken)
            => await _payableSvc.GetPayHistoryAsync(request.UserId);
    }
}
