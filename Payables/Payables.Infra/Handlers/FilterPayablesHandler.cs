﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Payables
{
    public class FilterPayablesHandler : IRequestHandler<FilterPayablesQuery, List<PayHistoryDTO>>
    {
        private readonly IPayablesService _payableSvc;

        public FilterPayablesHandler(IPayablesService payablesSvc)
        {
            _payableSvc = payablesSvc;
        }

        public async Task<List<PayHistoryDTO>> Handle(FilterPayablesQuery request, CancellationToken cancellationToken)
        {
            var payables = await _payableSvc.GetFilteredPayHistoryAsync(request.UserId, request.PayableId);

            return payables;
        }
    }
}
