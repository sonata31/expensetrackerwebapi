﻿using Common;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Payables
{
    public class LogPayableHandler : IRequestHandler<LogPayableCommand, Response>
    {
        private readonly IPayablesService _payableSvc;

        public LogPayableHandler(IPayablesService payablesSvc)
        {
            _payableSvc = payablesSvc;
        }

        public async Task<Response> Handle(LogPayableCommand request, CancellationToken cancellationToken)
        {
            var payable = new PayableLogModel
            {
                PaidFor = request.PaidFor,
                Amount = Convert.ToDecimal(request.Amount),
                CoverageFrom = request.CoverageFrom,
                StartYear = request.StartYear,
                CoverageTo = request.CoverageTo,
                EndYear = request.EndYear,
                ModeOfPayment = request.ModeOfPayment,
                DatePaid = request.DatePaid,
                ReferenceNo = request.ReferenceNo,
                PayableId = request.PayableId
            };

            return await _payableSvc.LogPayAsync(payable);
        }
    }
}
