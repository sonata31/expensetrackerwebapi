﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Payables
{
    public class GetPayablesHandler : IRequestHandler<GetPayablesQuery, List<PayablesDTO>>
    {
        private readonly IPayablesService _payableSvc;

        public GetPayablesHandler(IPayablesService payablesSvc)
        {
            _payableSvc = payablesSvc;
        }

        public async Task<List<PayablesDTO>> Handle(GetPayablesQuery request, CancellationToken cancellationToken)
            => await _payableSvc.GetPayablesAsync(request.UserId);
    }
}
