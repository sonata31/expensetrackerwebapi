﻿using Common;
using Core;

using AutoMapper;
using System;
using System.Threading.Tasks;
using System.Net;
using System.Collections.Generic;

namespace Payables
{
    public interface IPayablesService
    {
        Task<Response> AddPayablesAsync(PayablesModel model);
        Task<List<PayablesDTO>> GetPayablesAsync(long uid);
        Task<Response> LogPayAsync(PayableLogModel model);
        Task<List<PayHistoryDTO>> GetPayHistoryAsync(long uid);
        Task<List<PayHistoryDTO>> GetFilteredPayHistoryAsync(long uid, long payableId);
    }

    public class PayablesService : IPayablesService
    {
        private readonly IMapper _mapper;
        private readonly IPayablesRepository _repo;
        private Response _response;

        public PayablesService(
            IMapper mapper,
            IPayablesRepository repo,
            Response response)
        {
            _mapper = mapper;
            _repo = repo;
            _response = response;
        }

        public async Task<Response> AddPayablesAsync(PayablesModel model)
        {
            var payables = _mapper.Map<Payable>(model);
            payables.DateCreated = DateTime.UtcNow;

            await _repo.AddPayablesAsync(payables);

            _response.StatusCode = HttpStatusCode.OK;
            _response.Message = "New payable added.";

            return _response;
        }

        public async Task<List<PayablesDTO>> GetPayablesAsync(long uid)
            => await _repo.GetPayablesAsync(uid);

        public async Task<Response> LogPayAsync(PayableLogModel model)
        {
            var payableLog = _mapper.Map<PayableLogs>(model);
            payableLog.DateCreated = DateTime.UtcNow;

            await _repo.LogPayAsync(payableLog);

            _response.StatusCode = HttpStatusCode.OK;
            _response.Message = "Payable paid.";

            return _response;
        }

        public async Task<List<PayHistoryDTO>> GetPayHistoryAsync(long uid)
            => await _repo.GetPayHistoryAsync(uid);

        public async Task<List<PayHistoryDTO>> GetFilteredPayHistoryAsync(long uid, long payableId)
            => await _repo.GetFilteredPayHistoryAsync(uid, payableId);
    }
}
