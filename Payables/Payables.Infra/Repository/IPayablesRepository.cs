﻿using Core;
using Microsoft.Extensions.Configuration;
using Persistence;
using Persistence.Config;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Payables
{
    public interface IPayablesRepository
    {
        Task AddPayablesAsync(Payable model);
        Task<List<PayablesDTO>> GetPayablesAsync(long uid);
        Task LogPayAsync(PayableLogs model);
        Task<List<PayHistoryDTO>> GetPayHistoryAsync(long uid);
        Task<List<PayHistoryDTO>> GetFilteredPayHistoryAsync(long uid, long payableId);
    }

    public class PayablesRepository : IPayablesRepository
    {
        private readonly DbContext _db;
        private readonly IConfiguration _config;

        public PayablesRepository(DbContext db, IConfiguration config)
        {
            _db = db;
            _config = config;
        }

        public async Task AddPayablesAsync(Payable model)
        {
            _db.Payables.Add(model);
            await _db.SaveChangesAsync();
        }

        public async Task<List<PayablesDTO>> GetPayablesAsync(long uid)
            => QueriesService.QueryDb<PayablesDTO>(_config, "usp_GetPayables", new Dictionary<string, object> { { "UserId", uid } });

        public async Task LogPayAsync(PayableLogs model)
        {
            _db.PayableLogs.Add(model);
            await _db.SaveChangesAsync();
        }

        public async Task<List<PayHistoryDTO>> GetPayHistoryAsync(long uid)
            => QueriesService.QueryDb<PayHistoryDTO>(_config, "usp_GetPayHistory", new Dictionary<string, object> { { "UserId", uid } });

        public async Task<List<PayHistoryDTO>> GetFilteredPayHistoryAsync(long uid, long payableId)
            => QueriesService.QueryDb<PayHistoryDTO>(_config, "usp_FilterByPayable", new Dictionary<string, object> { { "UserId", uid }, { "PayableId", payableId } });
    }
}
