﻿using Common;
using Microsoft.Extensions.DependencyInjection;

namespace Payables.Config
{
    public static class BusinessModelsRegistration
    {
        public static IServiceCollection RegisterPayablesBusinessModels(this IServiceCollection services)
        {
            services.AddSingleton<Response>();

            return services;
        }
    }
}
