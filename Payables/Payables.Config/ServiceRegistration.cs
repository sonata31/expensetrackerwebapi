﻿using Microsoft.Extensions.DependencyInjection;

namespace Payables.Config
{
    public static class ServiceRegistration
    {
        public static IServiceCollection RegisterPayablesServices(this IServiceCollection services)
        {
            services.AddTransient<IPayablesRepository, PayablesRepository>();
            services.AddTransient<IPayablesService, PayablesService>();

            return services;
        }
    }
}
