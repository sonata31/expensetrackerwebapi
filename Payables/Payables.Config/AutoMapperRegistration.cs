﻿using Core;

using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace Payables.Config
{
    public class AutoMapperRegistration : Profile
    {
        public AutoMapperRegistration()
        {
            CreateMap<PayablesModel, Payable>();
            CreateMap<PayableLogModel, PayableLogs>();
        }
    }

    public static class MapperRegistration
    {
        public static IServiceCollection ConfigurePayablesMappingProfile(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(MapperRegistration));

            return services;
        }
    }
}
