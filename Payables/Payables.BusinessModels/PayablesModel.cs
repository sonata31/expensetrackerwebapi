﻿namespace Payables
{
    public class PayablesModel
    {
        public long UserId { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
    }
}
