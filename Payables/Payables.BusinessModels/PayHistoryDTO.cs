﻿using System;

namespace Payables
{
    public class PayHistoryDTO
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public string ModeOfPayment { get; set; }
        public DateTime DatePaid { get; set; }
        public string ReferenceNo { get; set; }
    }
}
