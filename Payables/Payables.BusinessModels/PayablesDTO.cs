﻿namespace Payables
{
    public class PayablesDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
    }
}
