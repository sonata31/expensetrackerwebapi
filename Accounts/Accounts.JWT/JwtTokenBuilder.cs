﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Accounts
{
    public class JwtTokenBuilder
    {
        private int _expiryInMinutes = 10;
        private IConfiguration _config;

        public JwtTokenBuilder(IConfiguration config)
        {
            _config = config;
        }

        public string GenerateToken(List<Claim> claims)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("JwtSettings:SecurityKey").Value));

            var token = new JwtSecurityToken(
                issuer: _config.GetSection("AppConfiguration:Issuer").Value,
                audience: _config.GetSection("AppConfiguration:Audience").Value,
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(_expiryInMinutes),
                signingCredentials: new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256)
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}