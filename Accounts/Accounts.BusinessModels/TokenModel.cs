﻿namespace Accounts
{
    public class TokenModel
    {
        public string AccessToken { get; set; }
        public long UserId { get; set; }
        public string Email { get; set; }
    }
}
