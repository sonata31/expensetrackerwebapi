﻿//using RentalsCoreApi31.Infrastructure.DataContext;
//using RentalsCoreApi31.Core.BusinessModels;
//using RentalsCoreApi31.Core.BusinessModels.DTO;
//using Accounts.DTO;

using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Dapper;

namespace Accounts
{
    public interface IUserRepository
    {
        //Task<IResponseModel> SaveUserAsync(ApplicationUser obj);
        //Task<ApplicationUserDTO> GetUserInfoAsync(long uid);
    }

    public class UserRepository : IUserRepository
    {
        //private DatabaseContext _db;
        ////private IResponseModel _response;
        //private ILoggerService _loggerService;
        private IConfiguration _config;

        public UserRepository(
            //DatabaseContext db,
            //IResponseModel response,
            //ILoggerService loggerService,
            IConfiguration config)
        {
            //_db = db;
            //_response = response;
            //_loggerService = loggerService;
            _config = config;
        }

        //public async Task<IResponseModel> SaveUserAsync(ApplicationUser obj)
        //{
        //    try
        //    {
        //        await _db.Users.AddAsync(obj);
        //        await _db.SaveChangesAsync();

        //        _response.Status = true;
        //        _response.Message = "Registration Successful.";

        //        return _response;
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggerService.Log("Register User", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        _response.Status = false;
        //        _response.Message = "Registration Error.";

        //        return _response;
        //    }
        //}

        //public async Task<ApplicationUserDTO> GetUserInfoAsync(long uid)
        //{
        //    try
        //    {
        //        using (var con = new SqlConnection(_config["Database:ConnectionString"]))
        //        {
        //            con.Open();

        //            var user = await con.QueryAsync<ApplicationUserDTO>("sp_GetUserInfo", new { UserId = uid }, commandType: CommandType.StoredProcedure);

        //            con.Close();

        //            return user.Count() > 0 ? user.FirstOrDefault() : null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //_loggerService.Log("Get User Info", ex.InnerException.Message, ex.Message, ex.StackTrace);

        //        return null;
        //    }
        //}
    }
}
