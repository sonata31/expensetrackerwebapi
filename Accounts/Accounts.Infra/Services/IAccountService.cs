﻿using Core;
using Common;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using System.Net;
using System.Linq;
using System.Collections.Generic;
using System.Security.Claims;
using AutoMapper;
using System;

namespace Accounts
{
    public interface IAccountService
    {
        Task<Response> RegisterAdminUserAsync(AdminUserRegisterModel model);
        //Task<Response> RegisterUserAsync(UserRegisterModel model);
        ////Task<IResponseModel> VerifyEmailAsync(string email);
        Task<TokenModel> VerifyUserAsync(UserLoginModel obj);
        //Task<ApplicationUserDTO> GetUserInfoAsync(long uid);
    }

    public class AccountService : IAccountService
    {
        private IConfiguration _config;
        private IUserRepository _repo;
        private IMapper _mapper;

        private UserManager<ApplicationUser> _user;
        private RoleManager<ApplicationRole> _role;

        public AccountService(
            IConfiguration config,
            IUserRepository repo,
            IMapper mapper,
            UserManager<ApplicationUser> user,
            RoleManager<ApplicationRole> role)
        {
            _config = config;
            _repo = repo;
            _mapper = mapper;
            _user = user;
            _role = role;
        }

        public async Task<Response> RegisterAdminUserAsync(AdminUserRegisterModel model)
        {
            var user = _mapper.Map<ApplicationUser>(model);

            bool roleExists = await _role.RoleExistsAsync("Admin");

            if (!roleExists) return new Response { StatusCode = HttpStatusCode.BadRequest, Message = "Admin registration failed." };

            IdentityResult result = await _user.CreateAsync(user, model.Password);

            if (!result.Succeeded) return new Response { StatusCode = HttpStatusCode.InternalServerError, Message = "Admin registration failed." };

            await _user.AddToRoleAsync(user, "Admin");
            return new Response { StatusCode = HttpStatusCode.OK, Message = "Admin registration successful." };
        }

        //public async Task<Response> RegisterUserAsync(UserRegisterModel model)
        //{
        //    ApplicationUser user = new ApplicationUser
        //    {
        //        UserName = model.Email,
        //        Name = model.Name,
        //        Email = model.Email
        //    };

        //    bool roleExists = await _role.RoleExistsAsync(model.Role);

        //    if (!roleExists) return new Response { StatusCode = HttpStatusCode.BadRequest, Message = "User registration failed." };

        //    IdentityResult result = await _user.CreateAsync(user, model.Password);

        //    if (!result.Succeeded) return new Response { StatusCode = HttpStatusCode.InternalServerError, Message = "User registration failed." };

        //    await _user.AddToRoleAsync(user, model.Role);
        //    return new Response { StatusCode = HttpStatusCode.OK, Message = "User registration successful." };
        //}

        //public async Task<IResponseModel> VerifyEmailAsync(string email)
        //{
        //    var user = await _repo.GetUserInfoAsync(email);

        //    if (user != null)
        //    {
        //        _response.Status = false;
        //        _response.Message = "Email already exists.";
        //    }
        //    else
        //    {
        //        _response.Status = true;
        //        _response.Message = "Email available.";
        //    }

        //    return _response;
        //}

        public async Task<TokenModel> VerifyUserAsync(UserLoginModel obj)
        {
            ApplicationUser user = await _user.FindByEmailAsync(obj.Email);
            List<Claim> claims = new List<Claim>();
            var token = new JwtTokenBuilder(_config);

            if (user == null) return null;

            bool isAuthenticated = await _user.CheckPasswordAsync(user, obj.Password);

            if (!isAuthenticated) return null;

            var roles = await _user.GetRolesAsync(user);
            claims.Add(new Claim(ClaimTypes.Role, roles.FirstOrDefault()));

            return new TokenModel
            {
                AccessToken = token.GenerateToken(claims),
                UserId = user.Id,
                Email = user.Email
            };
        }

        //public async Task<ApplicationUserDTO> GetUserInfoAsync(long uid)
        //{
        //    ApplicationUserDTO user = await _repo.GetUserInfoAsync(uid);

        //    return user ?? null;
        //}
    }
}