﻿using Common;
using MediatR;

namespace Accounts
{
    public class CreateAdminUserCommand : IRequest<Response>
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
