﻿using MediatR;

namespace Accounts
{
    public class LoginUserCommand : IRequest<TokenModel>
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
