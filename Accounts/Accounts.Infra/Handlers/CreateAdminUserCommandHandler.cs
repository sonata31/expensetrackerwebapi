﻿using Common;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Accounts
{
    public class CreateAdminUserCommandHandler : IRequestHandler<CreateAdminUserCommand, Response>
    {
        private readonly IAccountService _accountSvc;

        public CreateAdminUserCommandHandler(IAccountService accountSvc)
        {
            _accountSvc = accountSvc;
        }

        public async Task<Response> Handle(CreateAdminUserCommand request, CancellationToken cancellationToken)
        {
            var userModel = new AdminUserRegisterModel
            {
                Name = request.Name,
                Email = request.Email,
                Password = request.Password
            };

            return await _accountSvc.RegisterAdminUserAsync(userModel);
        }
    }
}
