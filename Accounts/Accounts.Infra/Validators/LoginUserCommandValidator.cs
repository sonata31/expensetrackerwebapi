﻿using FluentValidation;

namespace Accounts
{
    public class LoginUserCommandValidator : AbstractValidator<LoginUserCommand>
    {
        public LoginUserCommandValidator()
        {
            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Email field is mandatory.")
                .EmailAddress();

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Password field is mandatory")
                .MinimumLength(5)
                .WithMessage("Password field should be 5 characters minimum");
        }
    }
}
