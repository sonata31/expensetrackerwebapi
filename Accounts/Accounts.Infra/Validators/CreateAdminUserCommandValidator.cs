﻿using FluentValidation;

namespace Accounts
{
    public class CreateAdminUserCommandValidator : AbstractValidator<CreateAdminUserCommand>
    {
        public CreateAdminUserCommandValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage("Name field is mandatory.");

            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Email field is mandatory.")
                .EmailAddress();

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Password field is mandatory")
                .MinimumLength(5)
                .WithMessage("Password field should be 5 characters minimum");
        }
    }
}
