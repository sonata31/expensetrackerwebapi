﻿using Persistence.Config;
using Core;

using Microsoft.Extensions.DependencyInjection;

namespace Accounts.Config
{
    public static class IdentityRegistration
    {
        public static IServiceCollection RegisterIdentity(this IServiceCollection services)
        {
            services.AddIdentityCore<ApplicationUser>()
                .AddRoles<ApplicationRole>()
                .AddEntityFrameworkStores<DbContext>();

            return services;
        }
    }
}
