﻿using Core;

using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace Accounts.Config
{
    public class AutoMapperRegistration : Profile
    {
        public AutoMapperRegistration()
        {
            CreateMap<AdminUserRegisterModel, ApplicationUser>()
                .ForMember(e => e.Name, e => e.MapFrom(e => e.Name))
                .ForMember(e => e.UserName, e => e.MapFrom(e => e.Email))
                .ForMember(e => e.Email, e => e.MapFrom(e => e.Email));
        }
    }

    public static class MapperRegistration
    {
        public static IServiceCollection ConfigureAccountsMappingProfile(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(MapperRegistration));

            return services;
        }
    }
}
