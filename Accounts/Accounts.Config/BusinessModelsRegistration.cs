﻿using Common;
using Microsoft.Extensions.DependencyInjection;

namespace Accounts.Config
{
    public static class BusinessModelsRegistration
    {
        public static IServiceCollection RegisterAccountsBusinessModels(this IServiceCollection services)
        {
            services.AddSingleton<Response>();

            return services;
        }
    }
}
