﻿using Microsoft.Extensions.DependencyInjection;

namespace Accounts.Config
{
    public static class ServiceRegistration
    {
        public static IServiceCollection RegisterAccountsServices(this IServiceCollection services)
        {
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IAccountService, AccountService>();

            return services;
        }
    }
}
