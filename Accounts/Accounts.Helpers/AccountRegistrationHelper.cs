﻿using Microsoft.AspNetCore.Identity;

namespace Accounts
{
    public class AccountRegistrationHelper
    {
        static UserManager<ApplicationUser> userManager;

        public static IdentityResult RegisterAdmin(ApplicationUser user, string password)
        {
            return userManager.CreateAsync(user, password).Result;
        }
    }
}
