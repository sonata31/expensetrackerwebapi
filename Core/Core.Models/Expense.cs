﻿using System;

namespace Core
{
    public class Expense : BaseEntity
    {
        public string Name { get; set; }
        public string StorePurchased { get; set; }
        public DateTime DateOfPurchase { get; set; }
        public decimal Amount { get; set; }

        public ApplicationUser User { get; set; }
        public long UserId { get; set; }
    }
}
