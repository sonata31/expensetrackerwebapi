﻿using System;

namespace Core
{
    public class Savings : BaseEntity
    {
        public decimal Amount { get; set; }
        public DateTime DepositDate { get; set; }

        public ApplicationUser User { get; set; }
        public long UserId { get; set; }
    }
}
