﻿using System;

namespace Core
{
    public class PayableLogs : BaseEntity
    {
        public string PaidFor { get; set; }
        public decimal Amount { get; set; }
        public string CoverageFrom { get; set; }
        public int? StartYear { get; set; }
        public string CoverageTo { get; set; }
        public int? EndYear { get; set; }
        public string ModeOfPayment { get; set; }
        public DateTime DatePaid { get; set; }
        public string ReferenceNo { get; set; }

        public Payable Payables { get; set; }
        public long PayableId { get; set; }
    }
}
