﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Core
{
    public class ApplicationUser : IdentityUser<long>
    {
        public string Name { get; set; }
        public IEnumerable<Payable> Payables { get; set; }
        public IEnumerable<Expense> Expenses { get; set; }
        public IEnumerable<Savings> Savings { get; set; }
    }
}
