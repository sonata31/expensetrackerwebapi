﻿using System.Collections.Generic;

namespace Core
{
    public class Payable : BaseEntity
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public IEnumerable<PayableLogs> PayableLogs { get; set; }

        public ApplicationUser User { get; set; }
        public long UserId { get; set; }
    }
}
