﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Persistence.Config
{
    public class DbContextFactory : IDesignTimeDbContextFactory<DbContext>
    {
        public DbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DbContext>();
            builder.UseSqlServer("Data Source=SONATA31\\SQL2K19;Initial Catalog=ExpenseTrackerApiDB;User Id=sa;Password=demo123!;MultipleActiveResultSets=True");

            return new DbContext(builder.Options);
        }
    }
}
