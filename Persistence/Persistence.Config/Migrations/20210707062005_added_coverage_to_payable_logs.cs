﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Config.Migrations
{
    public partial class added_coverage_to_payable_logs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CoverageFrom",
                table: "Payables");

            migrationBuilder.DropColumn(
                name: "CoverageTo",
                table: "Payables");

            migrationBuilder.DropColumn(
                name: "EndYear",
                table: "Payables");

            migrationBuilder.DropColumn(
                name: "StartYear",
                table: "Payables");

            migrationBuilder.AddColumn<string>(
                name: "CoverageFrom",
                table: "PayableLogs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoverageTo",
                table: "PayableLogs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EndYear",
                table: "PayableLogs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StartYear",
                table: "PayableLogs",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1L,
                column: "ConcurrencyStamp",
                value: "8669f5c5-5dd6-477a-8628-9dbd647cee05");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CoverageFrom",
                table: "PayableLogs");

            migrationBuilder.DropColumn(
                name: "CoverageTo",
                table: "PayableLogs");

            migrationBuilder.DropColumn(
                name: "EndYear",
                table: "PayableLogs");

            migrationBuilder.DropColumn(
                name: "StartYear",
                table: "PayableLogs");

            migrationBuilder.AddColumn<string>(
                name: "CoverageFrom",
                table: "Payables",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoverageTo",
                table: "Payables",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EndYear",
                table: "Payables",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StartYear",
                table: "Payables",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1L,
                column: "ConcurrencyStamp",
                value: "56f0c4cf-ee86-4847-b9ec-ce01946e580b");
        }
    }
}
