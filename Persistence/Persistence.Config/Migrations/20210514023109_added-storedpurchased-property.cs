﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Config.Migrations
{
    public partial class addedstoredpurchasedproperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "StorePurchased",
                table: "Expenses",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1L,
                column: "ConcurrencyStamp",
                value: "692c82e4-69f6-4694-8f70-6d2ec1981c3b");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StorePurchased",
                table: "Expenses");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1L,
                column: "ConcurrencyStamp",
                value: "cd78bad2-fb92-45fe-b2f9-634b5df6eeb5");
        }
    }
}
