﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Config.Migrations
{
    public partial class addedpayablelogsentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NextDateDue",
                table: "Payables");

            migrationBuilder.CreateTable(
                name: "PayableLogs",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    PaidFor = table.Column<string>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    ModeOfPayment = table.Column<string>(nullable: false),
                    DatePaid = table.Column<DateTime>(nullable: false),
                    ReferenceNo = table.Column<string>(nullable: false),
                    PayableLogsId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PayableLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PayableLogs_Payables_PayableLogsId",
                        column: x => x.PayableLogsId,
                        principalTable: "Payables",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1L,
                column: "ConcurrencyStamp",
                value: "5259349e-2195-4576-a900-532e760ce1ea");

            migrationBuilder.CreateIndex(
                name: "IX_PayableLogs_PayableLogsId",
                table: "PayableLogs",
                column: "PayableLogsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PayableLogs");

            migrationBuilder.AddColumn<DateTime>(
                name: "NextDateDue",
                table: "Payables",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1L,
                column: "ConcurrencyStamp",
                value: "6d3967f6-fba3-4625-920b-888a6bf10388");
        }
    }
}
