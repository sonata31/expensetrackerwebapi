﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Config.Migrations
{
    public partial class changedpayablelogsidtopayableid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PayableLogs_Payables_PayableLogsId",
                table: "PayableLogs");

            migrationBuilder.DropIndex(
                name: "IX_PayableLogs_PayableLogsId",
                table: "PayableLogs");

            migrationBuilder.DropColumn(
                name: "PayableLogsId",
                table: "PayableLogs");

            migrationBuilder.AddColumn<long>(
                name: "PayableId",
                table: "PayableLogs",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1L,
                column: "ConcurrencyStamp",
                value: "a4b7e3d5-745e-4baf-8d93-2423a0247136");

            migrationBuilder.CreateIndex(
                name: "IX_PayableLogs_PayableId",
                table: "PayableLogs",
                column: "PayableId");

            migrationBuilder.AddForeignKey(
                name: "FK_PayableLogs_Payables_PayableId",
                table: "PayableLogs",
                column: "PayableId",
                principalTable: "Payables",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PayableLogs_Payables_PayableId",
                table: "PayableLogs");

            migrationBuilder.DropIndex(
                name: "IX_PayableLogs_PayableId",
                table: "PayableLogs");

            migrationBuilder.DropColumn(
                name: "PayableId",
                table: "PayableLogs");

            migrationBuilder.AddColumn<long>(
                name: "PayableLogsId",
                table: "PayableLogs",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1L,
                column: "ConcurrencyStamp",
                value: "5259349e-2195-4576-a900-532e760ce1ea");

            migrationBuilder.CreateIndex(
                name: "IX_PayableLogs_PayableLogsId",
                table: "PayableLogs",
                column: "PayableLogsId");

            migrationBuilder.AddForeignKey(
                name: "FK_PayableLogs_Payables_PayableLogsId",
                table: "PayableLogs",
                column: "PayableLogsId",
                principalTable: "Payables",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
