﻿using Core;

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Persistence.Config
{
    public class DbContext : IdentityDbContext<ApplicationUser, ApplicationRole, long>
    {
        public DbContext(DbContextOptions options) : base(options)
        {
        }

        public DbContext()
        {
        }

        public DbSet<Payable> Payables { get; set; }
        public DbSet<PayableLogs> PayableLogs { get; set; }
        public DbSet<Expense> Expenses { get; set; }
        public DbSet<Savings> Savings { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            #region Role Configuration
            builder.Entity<ApplicationRole>().HasData(
                new ApplicationRole { Id = 1, Name = "Admin", NormalizedName = "ADMIN" }
            );
            #endregion

            #region Expense Configuration
            builder.Entity<Expense>(e =>
            {
                e.Property(p => p.Name).IsRequired();
                e.Property(p => p.StorePurchased).IsRequired(false);
                e.Property(p => p.DateOfPurchase).IsRequired();
                e.Property(p => p.Amount).IsRequired();
                e.HasOne(p => p.User).WithMany(p => p.Expenses).HasForeignKey(p => p.UserId);
            });
            #endregion

            #region Savings Configuration
            builder.Entity<Savings>(e =>
            {
                e.Property(p => p.Amount).IsRequired();
                e.Property(p => p.DepositDate).IsRequired();
                e.HasOne(p => p.User).WithMany(p => p.Savings).HasForeignKey(p => p.UserId);
            });
            #endregion

            #region Payables Configuration
            builder.Entity<Payable>(e =>
            {
                e.Property(p => p.Name).IsRequired();
                e.Property(p => p.Amount).IsRequired();
                e.HasOne(p => p.User).WithMany(p => p.Payables).HasForeignKey(p => p.UserId);
            });
            #endregion

            #region Payables Logs Configuration
            builder.Entity<PayableLogs>(e =>
            {
                e.Property(p => p.PaidFor).IsRequired();
                e.Property(p => p.Amount).IsRequired();
                e.Property(p => p.CoverageFrom).IsRequired(false);
                e.Property(p => p.StartYear).IsRequired(false);
                e.Property(p => p.CoverageTo).IsRequired(false);
                e.Property(p => p.EndYear).IsRequired(false);
                e.Property(p => p.ModeOfPayment).IsRequired();
                e.Property(p => p.DatePaid).IsRequired();
                e.Property(p => p.ReferenceNo).IsRequired();
                e.HasOne(p => p.Payables).WithMany(p => p.PayableLogs).HasForeignKey(p => p.PayableId);
            });
            #endregion
        }
    }
}
