﻿using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;

namespace Persistence
{
    public class QueriesService
    {
        public static List<T> QueryDb<T>(IConfiguration config, string storedprocname, Dictionary<string, object> valuePairs)
        {
            try
            {
                using (var con = new SqlConnection(config["Database:ConnectionString"]))
                {
                    con.Open();

                    Dictionary<string, object> keyValues = new Dictionary<string, object>();

                    foreach (var item in valuePairs)
                        keyValues.Add(item.Key, item.Value);

                    var data = con.QueryAsync<T>(storedprocname, keyValues, commandType: CommandType.StoredProcedure).Result;

                    con.Close();

                    return data.AsList().Count > 0 ? data.AsList() : null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
