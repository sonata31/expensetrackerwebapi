﻿using System;

namespace Savings
{
    public class SavingsDTO
    {
        public decimal Amount { get; set; }
        public DateTime DepositDate { get; set; }
    }
}
