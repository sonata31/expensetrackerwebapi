﻿using System;

namespace Savings
{
    public class SavingsModel
    {
        public decimal Amount { get; set; }
        public DateTime DepositDate { get; set; }
        public long UserId { get; set; }
    }
}
