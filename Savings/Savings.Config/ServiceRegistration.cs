﻿using Microsoft.Extensions.DependencyInjection;

namespace Savings.Config
{
    public static class ServiceRegistration
    {
        public static IServiceCollection RegisterSavingsServices(this IServiceCollection services)
        {
            services.AddTransient<ISavingsRepository, SavingsRepository>();
            services.AddTransient<ISavingsService, SavingsService>();

            return services;
        }
    }
}
