﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace Savings.Config
{
    public class AutoMapperRegistration : Profile
    {
        public AutoMapperRegistration()
        {
            CreateMap<SavingsModel, Core.Savings>();
        }
    }

    public static class MapperRegistration
    {
        public static IServiceCollection ConfigureSavingsMappingProfile(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(MapperRegistration));

            return services;
        }
    }
}
