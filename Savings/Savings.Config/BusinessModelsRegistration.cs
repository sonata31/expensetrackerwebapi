﻿using Common;
using Microsoft.Extensions.DependencyInjection;

namespace Savings.Config
{
    public static class BusinessModelsRegistration
    {
        public static IServiceCollection RegisterSavingsBusinessModels(this IServiceCollection services)
        {
            services.AddSingleton<Response>();

            return services;
        }
    }
}
