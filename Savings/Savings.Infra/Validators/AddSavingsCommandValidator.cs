﻿using FluentValidation;
using Common.Validators;

namespace Savings
{
    public class AddSavingsCommandValidator : AbstractValidator<AddSavingsCommand>
    {
        public AddSavingsCommandValidator()
        {
            RuleFor(x => x.UserId)
                .NotEmpty()
                .WithMessage("Please provide user id.");

            RuleFor(x => x.Amount)
                .NotEmpty()
                .WithMessage("Amount field is mandatory.")
                .ValidateAmount();
        }
    }
}
