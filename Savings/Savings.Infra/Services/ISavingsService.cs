﻿using AutoMapper;
using Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Savings
{
    public interface ISavingsService
    {
        Task<Response> AddSavingsAsync(SavingsModel model);
        Task<List<SavingsDTO>> GetSavingsAsync(long uid);
    }

    public class SavingsService : ISavingsService
    {
        private readonly IMapper _mapper;
        private readonly ISavingsRepository _repo;
        private Response _response;

        public SavingsService(
            IMapper mapper,
            ISavingsRepository repo,
            Response response)
        {
            _mapper = mapper;
            _repo = repo;
            _response = response;
        }

        public async Task<Response> AddSavingsAsync(SavingsModel model)
        {
            var savings = _mapper.Map<Core.Savings>(model);
            savings.DateCreated = DateTime.UtcNow;

            await _repo.AddSavingsAsync(savings);

            _response.StatusCode = HttpStatusCode.OK;
            _response.Message = "Savings recorded.";

            return _response;
        }

        public async Task<List<SavingsDTO>> GetSavingsAsync(long uid)
            => await _repo.GetPayHistoryAsync(uid);
    }
}
