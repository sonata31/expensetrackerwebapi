﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Savings
{
    public class GetSavingsHandler : IRequestHandler<GetSavingsQuery, List<SavingsDTO>>
    {
        private readonly ISavingsService _savingsSvc;

        public GetSavingsHandler(ISavingsService savingsSvc)
        {
            _savingsSvc = savingsSvc;
        }

        public async Task<List<SavingsDTO>> Handle(GetSavingsQuery request, CancellationToken cancellationToken)
            => await _savingsSvc.GetSavingsAsync(request.UserId);
    }
}
