﻿using Common;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Savings
{
    public class AddSavingsHandler : IRequestHandler<AddSavingsCommand, Response>
    {
        private readonly ISavingsService _savingsSvc;

        public AddSavingsHandler(ISavingsService savingsSvc)
        {
            _savingsSvc = savingsSvc;
        }

        public async Task<Response> Handle(AddSavingsCommand request, CancellationToken cancellationToken)
        {
            var savings = new SavingsModel
            {
                UserId = request.UserId,
                Amount = Convert.ToDecimal(request.Amount),
                DepositDate = request.DepositDate
            };

            return await _savingsSvc.AddSavingsAsync(savings);
        }
    }
}
