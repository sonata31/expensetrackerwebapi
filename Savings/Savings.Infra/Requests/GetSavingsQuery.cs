﻿using MediatR;
using System.Collections.Generic;

namespace Savings
{
    public class GetSavingsQuery : IRequest<List<SavingsDTO>>
    {
        public long UserId { get; set; }
    }
}
