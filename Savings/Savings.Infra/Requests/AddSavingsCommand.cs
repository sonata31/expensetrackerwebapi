﻿using Common;
using MediatR;
using System;

namespace Savings
{
    public class AddSavingsCommand : IRequest<Response>
    {
        public double Amount { get; set; }
        public DateTime DepositDate { get; set; }
        public long UserId { get; set; }
    }
}
