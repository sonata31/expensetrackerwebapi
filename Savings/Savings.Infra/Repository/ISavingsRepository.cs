﻿using Microsoft.Extensions.Configuration;
using Persistence;
using Persistence.Config;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Savings
{
    public interface ISavingsRepository
    {
        Task AddSavingsAsync(Core.Savings model);
        Task<List<SavingsDTO>> GetPayHistoryAsync(long uid);
    }

    public class SavingsRepository : ISavingsRepository
    {
        private readonly DbContext _db;
        private readonly IConfiguration _config;

        public SavingsRepository(DbContext db, IConfiguration config)
        {
            _db = db;
            _config = config;
        }

        public async Task AddSavingsAsync(Core.Savings model)
        {
            _db.Savings.Add(model);
            await _db.SaveChangesAsync();
        }

        public async Task<List<SavingsDTO>> GetPayHistoryAsync(long uid)
            => QueriesService.QueryDb<SavingsDTO>(_config, "usp_GetSavings", new Dictionary<string, object> { { "UserId", uid } });
    }
}
